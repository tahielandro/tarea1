import { Injectable } from '@nestjs/common';
import axios from 'axios';

@Injectable()
export class CatService {
  private readonly apiUrl = 'https://api.thecatapi.com/v1';

  async gatos() {
    try {
      const respuesta = await axios.get(`${this.apiUrl}/images/search`);
      console.log(respuesta);
      const gatos = respuesta.data;
      return gatos;
    } catch (error) {
      throw new Error('Fallo en la conexion');
    }
  }
}
