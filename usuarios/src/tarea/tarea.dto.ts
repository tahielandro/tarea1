import { IsNumber, IsString } from 'class-validator';

export class TareaDto {
  @IsString()
  titulo: string;

  @IsString()
  descripcion: string;

  @IsString()
  estado: string;

  @IsNumber()
  idUsuario: number;
}
