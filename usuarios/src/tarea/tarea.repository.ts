import { Injectable } from '@nestjs/common';
import { Tarea } from '../usuario/entities/tarea.entity.ts';
import { DataSource } from 'typeorm';
import { TareaDto } from './tarea.dto';

@Injectable()
export class TareaRepository {
  constructor(private dataSource: DataSource) {}

  async crear(id: number, tarea: TareaDto): Promise<Tarea> {
    return await this.dataSource.getRepository(Tarea).save({
      titulo: tarea.titulo,
      descripcion: tarea.descripcion,
      estado: tarea.estado,
      idUsuario: id,
    });
  }
}
