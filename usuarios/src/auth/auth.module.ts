import { Module } from '@nestjs/common';
import { AuthService } from './auth.service';
import { AuthController } from './auth.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Usuario } from 'src/usuario/entities/usuario.entity';
import { UsuarioModule } from 'src/usuario/usuario.module';
import { JwtModule } from '@nestjs/jwt';
import { UsuarioService } from 'src/usuario/usuario.service';
import { UsuarioRepository } from 'src/usuario/usuario.repository';
import { jwtConstants } from 'src/constants/constant';
import { JwtStrategy } from './jwt.strategy';
import { TareaModule } from 'src/tarea/tarea.module';
import { CatmoduleModule } from 'src/catmodule/catmodule.module';
import { PerfilService } from 'src/usuario/perfil.service';
import { PerfilRepository } from 'src/usuario/perfil.repository';

@Module({
  imports: [
    TypeOrmModule.forFeature([Usuario]),
    UsuarioModule,
    TareaModule,
    CatmoduleModule,
    JwtModule.register({
      secret: jwtConstants.secret,
      signOptions: { expiresIn: '360000s' },
    }),
  ],
  providers: [
    AuthService,
    UsuarioService,
    UsuarioRepository,
    PerfilService,
    PerfilRepository,
    JwtStrategy,
  ],
  controllers: [AuthController],
})
export class AuthModule {}
