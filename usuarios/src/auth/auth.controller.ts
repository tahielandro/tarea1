import { Body, Controller, Post } from '@nestjs/common';
import { CredencialesDTO } from '../usuario/dto/credenciales.dto';
import { UsuarioService } from '../usuario/usuario.service';
import { AuthService } from './auth.service';
import { ApiTags } from '@nestjs/swagger';

@ApiTags('Auth controller')
@Controller('auth')
export class AuthController {
  constructor(
    private usuarioService: UsuarioService,
    private authService: AuthService,
  ) {}

  @Post('login')
  async login(@Body() credencialesDTO: CredencialesDTO) {
    console.log('credenciales del cuerpo', credencialesDTO);
    const usuario = await this.usuarioService.validarUsuario(
      credencialesDTO.nombreUsuario,
      credencialesDTO.password,
    );
    return this.authService.login(usuario);
  }
}
