import {
  Column,
  Entity,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Usuario } from './usuario.entity';
import { Categoria } from './categoria.entity';

@Entity()
export class Tarea {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  titulo: string;

  @Column()
  descripcion: string;

  @Column()
  estado: string;

  @Column({
    name: 'id_usuario',
  })
  idUsuario: number;

  @ManyToOne(() => Usuario, (usuario) => usuario.perfil)
  @JoinColumn({
    name: 'id_usuario',
    referencedColumnName: 'id',
  })
  usuario: Usuario;

  @ManyToMany(() => Categoria)
  @JoinTable()
  categorias: Categoria[];
}
