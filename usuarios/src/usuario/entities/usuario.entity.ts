import {
  Column,
  Entity,
  JoinColumn,
  OneToMany,
  OneToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Perfil } from './perfil.entity.ts';
import { Tarea } from './tarea.entity.ts.js';

@Entity()
export class Usuario {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: 'varchar', length: '30' })
  nombre: string;

  @Column({ type: 'varchar', length: '15', unique: true })
  nombreUsuario: string;

  @Column({ type: 'varchar', length: '50' })
  email: string;

  @Column({ type: 'varchar' })
  password: string;

  @OneToOne(() => Perfil, (perfil) => perfil.usuario)
  @JoinColumn()
  perfil: Perfil;

  @OneToMany(() => Tarea, (tarea) => tarea.usuario)
  tarea: Tarea[];

  constructor(data?: Partial<Usuario>) {
    if (data) Object.assign(this, data);
  }
}
