import { IsNumber, IsOptional, IsString } from 'class-validator';

export class PaginacionDto {
  @IsNumber()
  @IsOptional()
  limite?: number;

  @IsString()
  @IsOptional()
  orden?: 'ASC' | 'DESC';

  @IsNumber()
  @IsOptional()
  page?: number;

  @IsString()
  @IsOptional()
  estado?: string;
}
