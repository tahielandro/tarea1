import { IsNotEmpty, IsNumber, IsString } from 'class-validator';

export class PerfilDto {
  @IsString()
  @IsNotEmpty()
  foto: string;

  @IsNotEmpty()
  @IsNumber()
  idUsuario: number;
}
