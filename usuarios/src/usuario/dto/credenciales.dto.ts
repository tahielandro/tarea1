import { IsEmail, IsNotEmpty, IsString, MinLength } from 'class-validator';

export class CredencialesDTO {
  @IsString()
  @IsNotEmpty()
  @IsEmail()
  nombreUsuario: string;

  @IsString()
  @IsNotEmpty()
  @MinLength(6, {
    message: 'El nombre del usuario deberia tener al menos 5 caracteres',
  })
  password: string;
}
