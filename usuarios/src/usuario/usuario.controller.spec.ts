import { Test, TestingModule } from '@nestjs/testing';
import { UsuarioController } from './usuario.controller';
import { UsuarioService } from './usuario.service';
import { CreateUsuarioDto } from './dto/create-usuario.dto';
import { PaginacionDto } from './dto/paginacion.dto';

describe('UsuarioController', () => {
  let controller: UsuarioController;
  let usuarioService: UsuarioService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [UsuarioController],
      providers: [
        {
          provide: UsuarioService,
          useFactory: () => ({
            create: jest.fn(),
          }),
        },
      ],
    }).compile();

    controller = module.get<UsuarioController>(UsuarioController);
    usuarioService = module.get<UsuarioService>(UsuarioService);
  });

  it('Deberia crear un usuario: ', async () => {
    const usuario = new CreateUsuarioDto();
    await controller.create(usuario);
    expect(usuarioService.create).toHaveBeenCalledWith(usuario);
  });

  it('Deberia listar las tareas de un usuario', async () => {
    const id = 1;
    const paginacion = new PaginacionDto();
    const tareas = await usuarioService.listarTareas(id, paginacion);
    await controller.listarTareas(id, paginacion);
    expect(usuarioService.listarTareas).toHaveBeenCalledWith(id, paginacion);
    expect(await controller.listarTareas(id, paginacion)).toEqual(tareas);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
