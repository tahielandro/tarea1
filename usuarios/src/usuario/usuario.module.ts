import { Module } from '@nestjs/common';
import { UsuarioService } from './usuario.service';
import { UsuarioController } from './usuario.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Usuario } from './entities/usuario.entity';
import { UsuarioRepository } from './usuario.repository';
import { TareaModule } from 'src/tarea/tarea.module';
import { CatmoduleModule } from 'src/catmodule/catmodule.module';
import { Perfil } from './entities/perfil.entity.ts';
import { PerfilService } from './perfil.service';
import { PerfilRepository } from './perfil.repository';

@Module({
  imports: [
    TypeOrmModule.forFeature([Usuario, Perfil]),
    TareaModule,
    CatmoduleModule,
  ],
  controllers: [UsuarioController],
  providers: [
    UsuarioService,
    UsuarioRepository,
    PerfilService,
    PerfilRepository,
  ],
})
export class UsuarioModule {}
