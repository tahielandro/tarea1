import { Injectable } from '@nestjs/common';
import { Usuario } from './entities/usuario.entity';
import { DataSource } from 'typeorm';
import { CreateUsuarioDto } from './dto/create-usuario.dto';
import { UpdateUsuarioDto } from './dto/update-usuario.dto';
import { PaginacionDto } from './dto/paginacion.dto';

@Injectable()
export class UsuarioRepository {
  constructor(private dataSource: DataSource) {}

  async crear(createUsuarioDto: CreateUsuarioDto) {
    return await this.dataSource.getRepository(Usuario).save(createUsuarioDto);
  }

  async buscarPorId(id: number) {
    const usuario = await this.dataSource
      .getRepository(Usuario)
      .createQueryBuilder('usuario')
      .where('usuario.id = :id', { id })
      .getOne();
    return usuario;
  }

  async buscarPorNombre(nombreUsuario: string): Promise<Usuario> {
    const usuario = await this.dataSource.manager
      .createQueryBuilder(Usuario, 'usuario')
      .where('usuario.nombreUsuario = :nombre', { nombre: nombreUsuario })
      .getOne();
    return usuario;
  }

  async listar() {
    const usuarios = await this.dataSource
      .getRepository(Usuario)
      .createQueryBuilder('usuario')
      .getMany();
    return usuarios;
  }

  async actualizar(id: number, updateUsuarioDto: UpdateUsuarioDto) {
    await this.dataSource
      .createQueryBuilder()
      .update(Usuario)
      .set({ ...updateUsuarioDto })
      .where('id = :id', { id })
      .execute();
  }

  async eliminar(id: number) {
    await this.dataSource
      .createQueryBuilder()
      .delete()
      .from(Usuario)
      .where('id = :id', { id })
      .execute();
  }

  async listarTareas(id: number, paginacion: PaginacionDto) {
    const { limite, orden, estado } = paginacion;
    const tareas = this.dataSource
      .getRepository(Usuario)
      .createQueryBuilder('usuario')
      .leftJoinAndSelect('usuario.tarea', 'tarea')
      .leftJoinAndSelect('usuario.perfil', 'perfil')
      .select([
        'usuario.nombre',
        'perfil.foto',
        'tarea.titulo',
        'tarea.descripcion',
        'tarea.estado',
      ])
      .where('tarea.idUsuario = :id', { id })
      .take(limite);

    if (orden) {
      tareas.orderBy('tareas.titulo', orden);
    }

    if (estado) {
      tareas.andWhere('tarea.estado = :estado', { estado: estado });
    }

    return await tareas.getRawMany();
  }
}
