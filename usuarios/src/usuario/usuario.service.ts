import {
  ConflictException,
  Injectable,
  NotFoundException,
  UnauthorizedException,
} from '@nestjs/common';
import { CreateUsuarioDto } from './dto/create-usuario.dto';
// import { UpdateUsuarioDto } from './dto/update-usuario.dto';
import { UsuarioRepository } from './usuario.repository';
import { UpdateUsuarioDto } from './dto/update-usuario.dto';
import { Usuario } from './entities/usuario.entity';
import { TareaService } from 'src/tarea/tarea.service';
import { TareaDto } from 'src/tarea/tarea.dto';
// import { PaginacionDto } from './dto/paginacion.dto';
import { CatService } from 'src/catmodule/catservice.service';
import { PerfilService } from './perfil.service';
import { PerfilDto } from './dto/perfil.dto';
import { PaginacionDto } from './dto/paginacion.dto';

@Injectable()
export class UsuarioService {
  constructor(
    private readonly usuarioRepository: UsuarioRepository,
    private readonly tareaService: TareaService,
    private readonly gatosService: CatService,
    private readonly perfilService: PerfilService,
  ) {}

  async create(createUsuarioDto: CreateUsuarioDto): Promise<Usuario> {
    const usuarioExistente = await this.usuarioRepository.buscarPorNombre(
      createUsuarioDto.nombreUsuario,
    );
    if (usuarioExistente) {
      throw new ConflictException('Usuario Existente');
    }
    const usuario = await this.usuarioRepository.crear(createUsuarioDto);
    const imagen = await this.gatosService.gatos();
    const perfilDto: PerfilDto = {
      foto: imagen,
      idUsuario: usuario.id,
    };
    const perfil = await this.perfilService.crear(perfilDto);
    return { ...usuario, perfil: perfil };
  }

  async validarUsuario(
    nombreUsuario: string,
    password: string,
  ): Promise<Usuario> {
    const usuarioExistente =
      await this.usuarioRepository.buscarPorNombre(nombreUsuario);
    if (!usuarioExistente || usuarioExistente.password !== password) {
      console.log('Usuario Existente', usuarioExistente);
      throw new UnauthorizedException(
        'Nombre de Usuario o Contrasena incorrectos',
      );
    }
    return usuarioExistente;
  }

  findOne(id: number) {
    return this.usuarioRepository.buscarPorId(id);
  }

  async findAll() {
    return await this.usuarioRepository.listar();
  }

  actualizar(id: number, updateUsuarioDto: UpdateUsuarioDto) {
    const usuario = this.usuarioRepository.buscarPorId(id);
    if (!usuario) {
      throw new Error('Usuario con id ${id} no se ha encontrado');
    }
    return this.usuarioRepository.actualizar(id, updateUsuarioDto);
  }

  async eliminar(id: number) {
    return await this.usuarioRepository.eliminar(id);
  }

  async crearTarea(id: number, tarea: TareaDto) {
    const usuario = await this.usuarioRepository.buscarPorId(id);
    if (!usuario) {
      throw new NotFoundException('Usuario no encontrado');
    }
    return await this.tareaService.crear(usuario.id, tarea);
  }

  async listarTareas(id: number, paginacion: PaginacionDto) {
    const resultado = await this.usuarioRepository.listarTareas(id, paginacion);
    return resultado;
  }

  async gatos() {
    const resultado = await this.gatosService.gatos();
    return resultado;
  }
}
